POS OData Example
===

Requirements
---

- Visual Studio 2017
- DotNet Core 2.1

Instructions
---

Before running the application, open **Program.cs** and update `ClientId`, `Secret`, `TerminalKey`, and `ApiBaseUrl` (ex: `https://myposapi.com`).

Run the application by pressing `F5`.