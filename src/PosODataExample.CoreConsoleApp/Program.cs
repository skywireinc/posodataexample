﻿using Newtonsoft.Json;
using SkyWire.Pos.Model;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace PosODataExample.CoreConsoleApp
{
    class Program
    {
        /**
         * Update these values
         */

        private const string ClientId = "";
        private const string Secret = "";
        private const string TerminalKey = "";
        private const string ApiBaseUrl = "";

        private static readonly string ApiTerminalAuthUrl = $"{ApiBaseUrl}/api/terminalauth";
        private static readonly string ApiODataUrl = $"{ApiBaseUrl}/odata";

        static void Main(string[] args) => MainAsync(args).Wait();

        private static async Task MainAsync(string[] args)
        {
            var authObj = new
            {
                TerminalId = Guid.Empty,
                Token = string.Empty,
            };

            try
            {
                using (var client = new HttpClient())
                {
                    var authJson = JsonConvert.SerializeObject(
                        new
                        {
                            ClientId,
                            Secret,
                            TerminalKey,
                        });

                    client.DefaultRequestHeaders
                        .Accept
                        .Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var authResponse = await client.PostAsync(
                        ApiTerminalAuthUrl, 
                        new StringContent(authJson, Encoding.UTF8, "application/json"));

                    if (!authResponse.IsSuccessStatusCode)
                        throw new VerificationException(authResponse.StatusCode.ToString());

                    authObj = JsonConvert.DeserializeAnonymousType(
                        await authResponse.Content.ReadAsStringAsync(),
                        authObj);
                }

                var container = new Container(new Uri(ApiODataUrl));

                // When POS gets updated, your application may not have the latest schema.
                // This will allow you to query the data without any errors.
                container.IgnoreMissingProperties = true;

                // Add authorization header for every call
                container.SendingRequest2 += (_, e) =>
                {
                    e.RequestMessage.SetHeader("Authorization", $"Bearer {authObj.Token}");
                };

                /**
                 * If not using a .NET Core App, the below query could be executed like below:
                 * 
                 * var terminal = container.Terminals
                 *     .Where(t => t.Id == authObj.TerminalId)
                 *     .SingleOrDefault();
                 */
                var terminals = await container
                    .Terminals
                    .Where(t => t.Id == authObj.TerminalId)
                    .ExecuteQueryAsync();

                var terminal = terminals.SingleOrDefault();
            }
            catch (Exception exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(exception);
            }

            Console.Write("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
