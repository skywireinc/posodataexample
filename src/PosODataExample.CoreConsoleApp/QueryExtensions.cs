﻿using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Threading.Tasks;

namespace PosODataExample.CoreConsoleApp
{
    internal static class QueryExtensions
    {
        /// <summary>
        /// Executes OData query asynchronously.         
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="odataQuery">Query from the OData Container.</param>
        /// <returns>Query result of entities.</returns>
        /// <remarks>
        /// Core apps with the Microsoft.DataServices.Client lib will
        /// not let you execute synchronously. 
        /// https://stackoverflow.com/questions/33940030/how-to-asynchronously-consume-a-wcf-data-service-using-paging-with-beginexecute
        /// </remarks>
        public static async Task<IEnumerable<T>> ExecuteQueryAsync<T>(this IQueryable<T> odataQuery)
        {
            var query = odataQuery as DataServiceQuery<T>;
            if (query == null)
                throw new ArgumentException("Must be a query from the OData Container.");

            var factory = new TaskFactory<IEnumerable<T>>();

            return await factory.FromAsync(
                query.BeginExecute(null, null),
                asyncResult => query.EndExecute(asyncResult));
        }
    }
}
